{:deps {org.clojure/clojure {:mvn/version "1.10.1"}
        org.clojure/clojurescript {:mvn/version "1.10.773"}
        reagent {:mvn/version "0.10.0"}
        hipo {:mvn/version "0.5.2"}
        thheller/shadow-cljs {:mvn/version "2.10.19"}}}
